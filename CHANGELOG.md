# Changelog

## 4.1.0
This version adds a single feature (suggested in issue #5): keymaps.

- Added `selectShellKeymap` property in config (~/.hyper.js) to add your modifiers (i.e: 'ctrl+shift')
- Added `shortcut` property to `shells` to add the last key for your keymap (i.e: 'c')

(By using the two examples, that shell's keymap would be 'ctrl+shift+c')

## 4.0.0
This version adds several features, and does some breaking changes.

- Name tweaks: otherShells -> shells, shellName -> name, shellArgs -> args. Just for simplification.
- Notifications instead of radio buttons: This is needed to be able to add the next feature on the list.
- Shell groups: You can group similar shells, and have your shell menu more organized.
- Default shell: Selecting the default shell directly from your shells list lets you more flexibility when changing things.
- Auto-detect shells: If you dont specify a shells list, but set the `detectShells` flag to true,
it will automatically create a shells list according to your operating system.
- `env` support: Now you can also specify an env for each shell.
- Optional parameters for shells: `args` and `env` are now optional, and default to an empty array and object, respectively.
- Import/Export shells list: You can import or export your shells list through the JSON format.

## 3.0.0
This version adds no new features, but instead does two things:

- It fixes some issues reported by our users
- It does the necessary changes to function properly in Hyper 3.0.0

## 2.0.0
This version adds a major feature: Selectable shells.

In previous versions, you could only open new tabs with your provided shells.
But now, you can select which shell you want Hyper to use, and every time you open a new tab/window/etc,
it will automatically be opened using the shell you selected.

## 1.0.0
Initial version.

You can provide a number of shells, and open new tabs with them.
Now you dont have to change your shell in the config file every time you want to switch!