const myHeader = require("./app/header-component");
const Shell = require("./app/shell");

exports.decorateConfig = (cfg) => {
  if (this.shell) {
    //It makes sure that the shells are always bound correctly
    this.shell.bindShellToConfig(cfg);
  } else {
    // If there isn't a shell defined yet, start the plugin up
    this.shell = new Shell(cfg);
  }
  return cfg;
};

exports.decorateMenu = (menu) =>  {
    menu.push({type: 'separator'});
    menu.push({label: 'Shells', submenu: this.shell.menu});
    return menu;
}

exports.onWindow = (window) => {
  // options = {window, x, y, positioningItem, callback}
  window.rpc.on('open launch menu', (options = {}) => {
    this.shell.menu.popup(options);
  });
}

exports.decorateHeader = (Header, {React, notify}) => {
  // It allows the plugin to send notifications to the user
  rpc.on('notify', ({title, body, details}) => notify(title, body, details));
  
  return myHeader(Header, React);
}
