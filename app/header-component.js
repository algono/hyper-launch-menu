module.exports = (Header, React) => {
  return class extends React.Component {
    handleLaunchMenuClick(event) {
      rpc.emit('open launch menu');
    }

    render() {
      let props = Object.assign({}, this.props);
      const style = `
      .launch_menu {
        display: block;
        position: absolute;
        left: 4em;
        opacity: 0.5;
        width: 2em;
        z-index: 1000;
        -webkit-app-region: no-drag;
        cursor: default;
      }

      .launch_menu:hover {
        opacity: 1.0;
      }
      `
      const styleElem = React.createElement("style", {type: "text/css"}, style)
      props.customChildrenBefore =
        React.createElement(
          'div',
          {
            className: 'launch_menu header_shape.js',
            onClick: this.handleLaunchMenuClick
          },
          "++", props.customChildrenBefore, styleElem);
      return React.createElement(
        Header,
        Object.assign({}, props, {})
      );
    }
  }
}